package fr.polytechtours.projetsi.concert;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;

/**
 * Main class launching the app
 */
@SpringBootApplication
public class ConcertApplication {

	private static final Logger logger = LoggerFactory.getLogger(ConcertApplication.class);

	public static void main(String[] args) {
		SpringApplication concertApp = new SpringApplication(ConcertApplication.class);
		Environment env = concertApp.run(args).getEnvironment();
		logger.info("URL d'accès local : http://localhost:{}", env.getProperty("server.port"));

	}

}
