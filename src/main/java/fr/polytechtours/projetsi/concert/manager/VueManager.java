package fr.polytechtours.projetsi.concert.manager;

import fr.polytechtours.projetsi.concert.model.Concert;
import fr.polytechtours.projetsi.concert.model.Vue;
import fr.polytechtours.projetsi.concert.repository.VueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VueManager
{
    private VueRepository vueRepository;

    @Autowired
    public VueManager(VueRepository vueRepositoryParam)
    {
        this.vueRepository = vueRepositoryParam;
    }

    /**
     * Récupère une vue d'id "vueId"
     * @param vueId : l'id de la vue à récupérer
     * @return la vue correspondante à l'id "vueId"
     */
    public Vue getVueById(int vueId)
    {
        return vueRepository.findByIdVue(vueId);
    }

    /**
     * Ajoute une vue dans la BDD
     * @param vue : la vue à ajouter
     * @return la vue ajoutée
     */
    public Vue save(Vue vue)
    {
        return vueRepository.save(vue);
    }

    /**
     * Récupère les vues d'un concert d'id "concertId"
     * @param concertId : l'id du concert
     * @return les vues du concert d'id "concertId"
     */
    public List<Vue> getVueByConcertId(int concertId)
    {
        return vueRepository.findByIdConcert(concertId);
    }

    /**
     * Récupère la vue d'id "vueId" et d'un concert d'id "concertId"
     * @param concertId : l'id du concert
     * @param vueId : l'id de la vue recherchée
     * @return la vue trouvée correspondante
     */
    public Vue getVueByConcertIdAndVueId(int concertId, int vueId)
    {
        return vueRepository.findByIdConcertAndIdVue(concertId, vueId);
    }

    /**
     * Supprime une vue d'id "vueId"
     * @param vueId : l'id de la vue
     */
    public void deleteVueById(int vueId)
    {
        vueRepository.deleteById(vueId);
    }
}
