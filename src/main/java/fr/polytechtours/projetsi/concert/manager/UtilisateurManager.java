package fr.polytechtours.projetsi.concert.manager;

import fr.polytechtours.projetsi.concert.model.Utilisateur;
import fr.polytechtours.projetsi.concert.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UtilisateurManager
{
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    public UtilisateurManager(UtilisateurRepository utilisateurRepositoryParam)
    {
        this.utilisateurRepository = utilisateurRepositoryParam;
    }

    /**
     * Récupère l'utilisateur d'id "utilisateurId"
     * @param utilisateurId : l'id de l'utilisateur recherché
     * @return l'utilisateur correspondant à l'id utilisateurId
     */
    public Utilisateur getUtilisateurById(int utilisateurId)
    {
        return utilisateurRepository.findById(utilisateurId);
    }

    /**
     * Récupère un utilisateur par identifiant
     * @param identifiant : l'identifiant de l'utilisateur recherché
     * @return l'utilisateur correspondant à l'identifiant
     */
    public Utilisateur getUtilisateurByIdentifiant(String identifiant)
    {
        return utilisateurRepository.findByIdentifiant(identifiant);
    }

    /**
     * Ajoute un utilisateur
     * @param utilisateur : l'utilisateur à ajouter
     * @return l'utilisateur ajouté
     */
    public Utilisateur save(Utilisateur utilisateur)
    {
        return utilisateurRepository.save(utilisateur);
    }

    /**
     * Supprime un utilisateur d'id "utilisateurId"
     * @param utilisateurId : l'id de l'utilisateur à supprimer
     */
    public void deleteUtilisateurById(int utilisateurId)
    {
        utilisateurRepository.deleteById(utilisateurId);
    }

    /**
     * Récupère tous les utilisateurs présents dans la BDD
     * @return la liste des utilisateurs présents dans la BDD
     */
    public List<Utilisateur> getAllUtilisateurs()
    {
        return utilisateurRepository.findAll();
    }
}
