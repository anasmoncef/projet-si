package fr.polytechtours.projetsi.concert.manager;

import fr.polytechtours.projetsi.concert.model.Concert;
import fr.polytechtours.projetsi.concert.repository.ConcertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConcertManager
{
    private ConcertRepository concertRepository;

    @Autowired
    public ConcertManager(ConcertRepository concertRepositoryParam)
    {
        this.concertRepository = concertRepositoryParam;
    }

    /**
     * Récupère un concert par id
     * @param concertId : l'id du concert à récupérer
     * @return le concert correspondant au concertId
     */
    public Concert getConcertById(int concertId)
    {
        return concertRepository.findByIdConcert(concertId);
    }

    /**
     * Ajoute un concert dans la BDD
     * @param concert : le concert à ajouter
     * @return le concert ajouté
     */
    public Concert save(Concert concert)
    {
        return concertRepository.save(concert);
    }

    /**
     * Supprime un concert d'id "concertId"
     * @param concertId : l'id du concert à supprimer
     */
    public void deleteConcertById(int concertId)
    {
        concertRepository.deleteByIdConcert(concertId);
    }

    /**
     * Récupère tous les concerts de la BDD
     * @return la liste des concerts présents dans la BDD
     */
    public List<Concert> getAllConcerts()
    {
        return concertRepository.findAll();
    }
}
