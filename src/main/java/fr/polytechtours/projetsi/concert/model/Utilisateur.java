package fr.polytechtours.projetsi.concert.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Classe modèle représentant l'objet Utilisateur
 */
@Entity
@Table(name = "utilisateur")
public class Utilisateur implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "identifiant")
    private String identifiant;

    @Column(name = "nomUtilisateur")
    private String nomUtilisateur;

    @Column(name = "mdpUtilisateur")
    private String mdpUtilisateur;

    @Column(name = "isAdmin")
    private Boolean isAdmin = false;

    public Utilisateur() {

    }

    public Utilisateur(String identifiant, String nomUtilisateur, String mdpUtilisateur) {
        this.identifiant = identifiant;
        this.nomUtilisateur = nomUtilisateur;
        this.mdpUtilisateur = mdpUtilisateur;
        this.isAdmin = false;
    }

    public Utilisateur(String identifiant, String nomUtilisateur, String mdpUtilisateur, Boolean isAdmin) {
        this.identifiant = identifiant;
        this.nomUtilisateur = nomUtilisateur;
        this.mdpUtilisateur = mdpUtilisateur;
        this.isAdmin = isAdmin;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getMdpUtilisateur() {
        return mdpUtilisateur;
    }

    public void setMdpUtilisateur(String mdpUtilisateur) {
        this.mdpUtilisateur = mdpUtilisateur;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}
