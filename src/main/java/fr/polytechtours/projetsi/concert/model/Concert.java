package fr.polytechtours.projetsi.concert.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Classe modèle représentant l'objet Concert
 */
@Entity
@Table(name = "concert")
public class Concert implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idConcert")
    private Integer idConcert;

    @Column(name = "titreConcert")
    private String titreConcert;

    @Column(name = "dateConcert")
    private LocalDate dateConcert;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idConcert", cascade = CascadeType.ALL)
    private List<Vue> vueConcert;

    public Concert() {

    }

    public Concert(String titreConcert, LocalDate dateConcert) {
        this.titreConcert = titreConcert;
        this.dateConcert = dateConcert;
    }

    public Integer getIdConcert() {
        return idConcert;
    }

    public void setIdConcert(Integer idConcert) {
        this.idConcert = idConcert;
    }

    public String getTitreConcert() {
        return titreConcert;
    }

    public void setTitreConcert(String titreConcert) {
        this.titreConcert = titreConcert;
    }

    public LocalDate getDateConcert() {
        return dateConcert;
    }

    public void setDateConcert(LocalDate dateConcert) {
        this.dateConcert = dateConcert;
    }

    public List<Vue> getVueConcert() {
        return vueConcert;
    }

    public void setVueConcert(List<Vue> vueConcert) {
        this.vueConcert = vueConcert;
    }
}
