package fr.polytechtours.projetsi.concert.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Classe modèle représentant l'objet Vue
 */
@Entity
@Table(name = "vue")
public class Vue implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idVue")
    private Integer idVue;

    @Column(name = "nomVue")
    private String nomVue;

    @Column(name = "lienVue")
    private String lienVue;

    //@ManyToOne @OnDelete(action = OnDeleteAction.CASCADE)
    @Column(name="idConcert")
    private int idConcert;

    @Column(name = "description")
    private String description;

    public Vue() {

    }

    public Vue(String nomVue, String lienVue, int idConcert) {
        this.nomVue = nomVue;
        this.lienVue = lienVue;
        this.idConcert = idConcert;
    }

    public Vue(String nomVue, String lienVue, int idConcert, String description) {
        this.nomVue = nomVue;
        this.lienVue = lienVue;
        this.idConcert = idConcert;
        this.description = description;
    }

    public Vue(String nomVue, int idConcert) {
        this.nomVue = nomVue;
        this.idConcert = idConcert;
    }

    public Vue(String nomVue, int idConcert, String description) {
        this.nomVue = nomVue;
        this.idConcert = idConcert;
        this.description = description;
    }

    public Integer getIdVue() {
        return idVue;
    }

    public void setIdVue(Integer idVue) {
        this.idVue = idVue;
    }

    public String getNomVue() {
        return nomVue;
    }

    public void setNomVue(String nomVue) {
        this.nomVue = nomVue;
    }

    public String getLienVue() {
        return lienVue;
    }

    public void setLienVue(String lienVue) {
        this.lienVue = lienVue;
    }

    public int getIdConcert() {
        return idConcert;
    }

    public void setIdConcert(int idConcert) {
        this.idConcert = idConcert;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
