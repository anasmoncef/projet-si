package fr.polytechtours.projetsi.concert.controllers;

import fr.polytechtours.projetsi.concert.manager.UtilisateurManager;
import fr.polytechtours.projetsi.concert.model.Utilisateur;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class UtilisateurController
{
    @Resource
    UtilisateurManager utilisateurManager;

    /**
     * Récupère l'utilisateur d'id "id"
     * @param id : l'id de l'utilisateur
     * @return l'utilisateur trouvé dans la base de données
     */
    @GetMapping("/utilisateur/{id}")
    public Utilisateur getUtilisateurById(@PathVariable int id)
    {
        return utilisateurManager.getUtilisateurById(id);
    }

    /**
     * Récupère tous les utilisateurs présents dans la base de données
     * @return tous les utilisateurs de la base de données
     */
    @GetMapping("/utilisateurs")
    public List<Utilisateur> getAllUtilisateurs()
    {
        return utilisateurManager.getAllUtilisateurs();
    }

    /**
     * Ajoute un utilisateur dans la base de données
     * @param newUtilisateur : le nouvel utilisateur à ajouter
     * @return l'utilisateur ajouté
     */
    @PostMapping("/saveutilisateur")
    public Utilisateur saveUtilisateur(@RequestBody Utilisateur newUtilisateur)
    {
        return utilisateurManager.save(newUtilisateur);
    }

    /**
     * Supprime un utilisateur d'id "id"
     * @param id : l'id de l'utilisateur à supprimer
     * @return true si l'utilisateur est supprimé, false sinon
     */
    @PostMapping("/deleteutilisateur")
    public boolean deleteUtilisateurById(@RequestParam int id)
    {
        Utilisateur utilisateurToDelete = utilisateurManager.getUtilisateurById(id);
        if(utilisateurToDelete != null)
        {
            if (!utilisateurToDelete.getAdmin())
            {
                utilisateurManager.deleteUtilisateurById(id);
                return true;
            }
        }
        return false;
    }

    /**
     * Authentification d'un utilisateur avec un identifiant et un mot de passe
     * @param identifiant : l'identifiant que l'utilisateur a entré
     * @param motDePasse : le mot de passe que l'utilisateur a entré
     * @return true si l'identifiant et le mot de passe correspondent aux identifiants de l'utilisateur, false sinon
     */
    @PostMapping("/login")
    public boolean login(@RequestBody String identifiant, @RequestBody String motDePasse)
    {
        Utilisateur utilisateur = utilisateurManager.getUtilisateurByIdentifiant(identifiant);
        if(utilisateur != null)
        {
            return utilisateur.getMdpUtilisateur().equals(motDePasse);
        }
        return false;
    }

    /**
     * Récupère les informations de l'utilisateur d'identifiant "identifiant"
     * @param identifiant : l'identifiant de l'utilisateur
     * @return les informations de l'utilisateur correspondant
     */
    @GetMapping("/infoutilisateur/{identifiant}")
    public Utilisateur getInfoUtilisateurConnected(@PathVariable String identifiant)
    {
        return utilisateurManager.getUtilisateurByIdentifiant(identifiant);
    }

    /**
     * Vérifie si l'identifiant que l'utilisateur a entré existe
     * @param identifiant : l'identifiant que l'utilisateur a entré
     * @return true si l'identifiant existe, false sinon
     */
    @GetMapping("/identifiantexists/{identifiant}")
    public boolean checkIfIdentifiantExists(@PathVariable String identifiant)
    {
        return utilisateurManager.getUtilisateurByIdentifiant(identifiant) != null;
    }
}
