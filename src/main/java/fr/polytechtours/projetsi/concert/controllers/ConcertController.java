package fr.polytechtours.projetsi.concert.controllers;

import fr.polytechtours.projetsi.concert.manager.ConcertManager;
import fr.polytechtours.projetsi.concert.manager.VueManager;
import fr.polytechtours.projetsi.concert.model.Concert;
import fr.polytechtours.projetsi.concert.model.Vue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@RestController
public class ConcertController
{
    private static final Logger logger = LoggerFactory.getLogger(ConcertController.class);

    @Value("${vue.upload.folder}")
    private String uploadFolder;

    @Resource
    ConcertManager concertManager;

    @Resource
    VueManager vueManager;

    /**
     * Récupère les concerts par id
     * @param id : l'id du concert
     * @return le concert trouvé dans la base de données
     */
    @GetMapping("/concert/{id}")
    public Concert getConcertById(@PathVariable int id)
    {
        return concertManager.getConcertById(id);
    }

    /**
     * Récupère tous les concerts
     * @return tous les concerts présents dans la base de données
     */
    @GetMapping("/concert")
    public List<Concert> getAllConcerts()
    {
        return concertManager.getAllConcerts();
    }

    /**
     * Ajoute un concert dans la base de données
     * @param newConcert : le concert à ajouter
     * @return le concert ajouté
     */
    @PostMapping("/saveconcert")
    public Concert saveConcert(@RequestBody Concert newConcert)
    {
       return concertManager.save(newConcert);
    }

    /**
     * Récupère une vue d'idVue dans le concert idConcert
     * @param idConcert : l'id du concert
     * @param idVue : l'id de la vue
     * @return la vue d'idVue dans le concert idConcert
     */
    @GetMapping("/concert/{idConcert}/{idVue}")
    public Vue getVueByConcertIdAndById(@PathVariable int idConcert, @PathVariable int idVue)
    {
        return vueManager.getVueByConcertIdAndVueId(idConcert, idVue);
    }

    /**
     * Récupère toutes les vues d'un concert d'idConcert
     * @param idConcert : l'id du concert
     * @return toutes les vues du concert d'idConcert
     */
    @GetMapping("/concert/{idConcert}/all")
    public List<Vue> getAllVueByIdConcert(@PathVariable int idConcert)
    {
        return vueManager.getVueByConcertId(idConcert);
    }

    /**
     * Supprime la vue d'idVue
     * @param id : l'id de la vue à supprimer
     * @return true si la vue est supprimée, false sinon
     */
    @PostMapping("/deletevue/{id}")
    public boolean deleteVueById(@PathVariable int id)
    {
        Vue vueToDelete = vueManager.getVueById(id);
        if(vueToDelete != null)
        {
            vueManager.deleteVueById(id);
            return true;
        }
        return false;
    }

    /**
     * Ajoute une vue avec une vidéo
     * @param newVue : l'objet Vue à ajouter
     * @param video : la vidéo correspondante à la vue
     * @return la vue ajoutée
     */
    @RequestMapping(value = "savevue", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Vue saveVue(@RequestParam(value = "newVue") Object newVue, @RequestParam(value = "video") MultipartFile video)
    {
        //Récupération de l'objet newVue de JSON vers objet Java
        JsonReader reader = Json.createReader(new StringReader((String) newVue));
        JsonObject vueJson = reader.readObject();
        //Récupération des champs de l'objet vue JSON
        String nomVue = String.valueOf(vueJson.getInt("nomVue"));
        int idConcert = vueJson.getInt("idConcert");
        String descriptionVue = vueJson.getString("description");

        //Construction du chemin et du nom de la vue
        String extension = video.getOriginalFilename().substring(video.getOriginalFilename().lastIndexOf('.'));
//        String folder = "src/main/webapp/concerts/" + idConcert;
        String folder = uploadFolder + idConcert;
        String videoName =  nomVue + extension;

        Vue vue = new Vue(nomVue, idConcert, descriptionVue);
        vue.setLienVue(videoName);

        try {
            //Création du répertoire où seront stockés les vues (si le répertoire existe déja, pas d'effet)
            Files.createDirectories(Paths.get(folder));
            FileOutputStream fos = new FileOutputStream(folder + "/" + videoName);
            fos.write(video.getBytes()); //écriture du fichier vidéo dans le dossier de destination
            fos.close();

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return vueManager.save(vue);
    }

}
