package fr.polytechtours.projetsi.concert.repository;

import fr.polytechtours.projetsi.concert.model.Vue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * JPA Repository for Vue objects
 */
public interface VueRepository extends JpaRepository<Vue, Integer> {
    Vue findByIdVue(int vueId);
    Vue save(Vue vue);
    List<Vue> findByIdConcert(int idConcert);
    Vue findByIdConcertAndIdVue(int idConcert, int vueId);
}
