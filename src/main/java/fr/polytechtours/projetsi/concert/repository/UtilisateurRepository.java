package fr.polytechtours.projetsi.concert.repository;

import fr.polytechtours.projetsi.concert.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * JPA Repository for Utilisateur objects
 */
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer>
{
    Utilisateur findById(int utilisateurId);
    Utilisateur findByIdentifiant(String utilisateurIdentifiant);
    List<Utilisateur> findAll();
    Utilisateur save(Utilisateur utilisateur);
    void deleteById(int utilisateurId);
}
