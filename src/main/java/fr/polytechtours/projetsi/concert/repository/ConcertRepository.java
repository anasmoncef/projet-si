package fr.polytechtours.projetsi.concert.repository;

import fr.polytechtours.projetsi.concert.model.Concert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * JPA Repository for Concert objects
 */
public interface ConcertRepository extends JpaRepository<Concert, Integer>
{
    Concert findByIdConcert(int concertId);
    List<Concert> findAll();
    Concert save(Concert concert);
    void deleteByIdConcert(int concertId);
}
