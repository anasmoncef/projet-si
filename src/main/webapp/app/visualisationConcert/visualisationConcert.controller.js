myApp.controller('VisualisationConcertController', ['$scope', '$rootScope', '$state', function ($scope, $rootScope, $state,) {

    var idConcert = $rootScope.concertToShow;

    var options = {
        playbackRates: [1, 1.5, 2],
        muted: false,
    };

    var video = videojs('videoplay', options);
    // var pathConcert = "concerts/" + idConcert + "/1.mov";
    var pathConcert = "http://upload-concert/" + idConcert + "/1.mov";
    video.src({type:"video/mp4", src: pathConcert});
    video.load();

    $('#overlays-wrap').appendTo($('#videoplay'));

    video.ready(function()
    {
        this.on('fullscreenchange',function() {


            console.log('fullscreen event');
        });
    }); // end video.ready


    // OVERLAY PREPARE
    // $('#overlays-wrap, .overlay-item').hide();

    // OVERLAY INTERACTION
    $('.radio-tile').click( function() {
        // only show if not yet submitted, prevents submit then click again on radio-tile which would show the vtask-btn-continue
        var submitted = $(this).closest('.overlay-item').hasClass('overlay-submitted');
        if(!submitted)
        {
            $(this).parent().parent().next('.vtask-btn-continue').css('visibility', 'visible');
            // make sure we check the radio button
            $(this).prev('.vtask-choice').prop('checked', true);
        }
    });

    $('.vtask-btn-continue').click( function() {
        // hide continue button
        $(this).css('visibility', 'hidden');

        var overlay = $(this).closest('.overlay-item');
        overlay.hide();

    });

    sessionStorage.setItem("currentList",[2,3,4,5,6,7,8]);

}]);

function changeVideo(id){
    var video = videojs('videoplay');
    var currentTime = video.currentTime();
    var listeOrigine = [2,3,4,5,6,7,8];
    var listeCourante = sessionStorage.getItem("currentList").split(",");

    var idVideo = listeCourante[id-1];
    listeCourante = listeOrigine;
    listeCourante[listeCourante.indexOf(parseInt(idVideo,10))] = 1;
    sessionStorage.setItem("currentList",listeCourante);

    var path = video.currentSrc();
    path = path.substring(0,path.lastIndexOf("/")+1);
    path += idVideo + ".mov";

    video.src(path);
    video.load();
    video.currentTime(currentTime);

}
