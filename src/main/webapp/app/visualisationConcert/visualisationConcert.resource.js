'use strict';

myApp.factory('GetConcert', function ($resource) {
    return $resource('/concert/:id', {id: '@id'}, {
        'get': {
            method: 'GET'
        }
    })
});