myApp.factory('AjoutConcert', function ($resource) {
    return $resource('/saveconcert', {}, {
        'post': {
            method: 'POST',
            isArray: false
        }
    });
});
