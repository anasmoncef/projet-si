myApp.controller('AjoutConcertController', ['$scope', '$rootScope', '$state', 'AjoutConcert', 'AjoutVue', function ($scope, $rootScope, $state, AjoutConcert, AjoutVue) {

    $rootScope.concertToShow = -1;
    $scope.files = [];
    $scope.listeVue = [];
    $scope.donneesVue = [];
    $scope.vueAAjouter = {};
    $scope.fichierVue = {};

    // $scope.("#datepicker").datepicker({
    //     altField: "yy/mm/dd"
    // });

    $scope.validerPage = function () {
        $scope.aEnvoyer = {
            'titreConcert' : $scope.titreConcert,
            'dateConcert' : $scope.parseDateToLocalDateJava($scope.dateConcert)
        };
        AjoutConcert.post($scope.aEnvoyer, function (result, headers) {
            $scope.headers = headers;
        }).$promise
            .then(function (result) {
                $scope.idConcert = result.idConcert;
                for (var i = 0; i < $scope.listeVue.length; i++) {
                    $scope.formData = new FormData();
                    $scope.newVue = {
                        'nomVue' : $scope.donneesVue[i].numeroVue,
                        'idConcert': $scope.idConcert,
                        'description' : $scope.donneesVue[i].description
                    };
                    $scope.formData.append('newVue', JSON.stringify($scope.newVue));
                    $scope.formData.append('video', $scope.listeVue[i]);

                    AjoutVue.post($scope.formData, function (result, headers) {
                        $scope.headers = headers;
                    }).$promise
                        .then(function (result) {
                            $rootScope.concertToShow = $scope.idConcert;
                            $state.go('visualisationConcert');
                        });
                }
        });

    };

    $scope.ajoutVue = function () {
        var fichier = document.getElementById('fichier').files[0];
        $scope.listeVue.push(fichier);
        var nbVue = $scope.listeVue.length;
        var contenuVue = {
            'numeroVue': nbVue,
            'description': $scope.descriptionVue
        };
        $scope.donneesVue.push(contenuVue);
        $scope.descriptionVue = "";
        $scope.files = "";
        document.getElementById('fichier').value = "";
    };

    $scope.supprimerVue = function (vue) {
        var index = $scope.listeVue.indexOf(vue);
        if (index > -1) {
            $scope.listeVue.splice(index, 1);
            $scope.donneesVue.splice(index, 1);
            for (var i = index; i < $scope.donneesVue.length; i++) {
                $scope.donneesVue[i].numeroVue = $scope.donneesVue[i].numeroVue -1;
            }
        }
        document.getElementById('fichier').value = "";
    };

    $scope.parseDateToLocalDateJava = function (strDate) {
        day = strDate.substring(0,2);
        month = strDate.substring(3,5);
        year = strDate.substring(6,10);
        return year + "-" + month + "-" + day;
    }
}]);