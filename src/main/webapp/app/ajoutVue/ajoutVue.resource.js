myApp.factory('AjoutVue', function ($resource) {
    return $resource('/savevue', {}, {
        'post': {
            method: 'POST',
            isArray: false,
            transformRequest: angular.identity,
            headers: {'Content-Type' : undefined}
        }
    });
});