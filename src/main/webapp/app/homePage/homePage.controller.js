myApp.controller('HomePageController', ['$scope', '$rootScope', '$state', 'GetAllConcert', function ($scope, $rootScope, $state, GetAllConcert) {
    $scope.listeConcert = [];
    $scope.listeDescriptionVue = [];
    $rootScope.concertToShow = -1;
    GetAllConcert.get().$promise
        .then(function (result) {
            angular.forEach(result, function (concert) {
                $scope.listeConcert.push(concert);
                var descriptionVue = "";
                for (var i = 0; i < concert.vueConcert.length; i++) {
                    descriptionVue = descriptionVue + concert.vueConcert[i].description + ", ";
                }
                descriptionVue = descriptionVue.substring(0,descriptionVue.lastIndexOf(","));
                $scope.listeDescriptionVue.push(descriptionVue);
            });
        });

    $scope.goVisualisation = function (id) {
        $rootScope.concertToShow = id;
        $state.go('visualisationConcert');
    }

}]);