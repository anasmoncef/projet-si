myApp.factory('GetAllConcert', function ($resource) {
    return $resource('/concert', {}, {
        'get' : {
            method: 'GET',
            isArray: true,
            transformResponse: function (data) {
                data = angular.fromJson(data);
                return data;
            }
        }
    })
});