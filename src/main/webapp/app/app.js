'use strict';

var concertApp = angular.module('concertApp', ['ui.router']);
concertApp.config(function($stateProvider) {
    var pageVideoJs = {
        name: 'pageVideoJs',
        url: '/#/pageVideoJs',
        templateUrl: 'index.html'
    };

    $stateProvider.state(pageVideoJs);
});
    // .run(function ($rootScope, $anchorScroll, $timeout, $location, $window, $http, $state) {
    //
    // })
    //
    // .config(function ($provide, $stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
    //     $urlRouterProvider.otherwise('/');
    //     $stateProvider.state('site', {
    //         'abstract': true,
    //         views: {
    //             '@': {
    //                 templateUrl: 'app/homePage/homePage.html',
    //                 controller: 'HomePageController'
    //             }
    //         }
    //     })
    // })
