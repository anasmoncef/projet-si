var myApp = angular.module('concertApp', ['ui.router', 'ngResource']);

myApp.config(function($stateProvider,$urlRouterProvider) {
    $urlRouterProvider.otherwise("/")

    var homePageState = {
        name: 'homePage',
        url: '/',
        templateUrl: '/app/homePage/homePage.html',
        controller: 'HomePageController'
    };

    var ajoutConcertState = {
        name: 'ajoutConcert',
        url: '/ajoutConcert',
        templateUrl: '/app/ajoutConcert/ajoutConcert.html',
        controller: 'AjoutConcertController'
    };

    var visualisationConcertState = {
        name: 'visualisationConcert',
        url: '/visualisationConcert/:idConcert',
        templateUrl: '/app/visualisationConcert/visualisationConcert.html',
        controller:'VisualisationConcertController'
    };

    $stateProvider.state(homePageState);
    $stateProvider.state(ajoutConcertState);
    $stateProvider.state(visualisationConcertState);
});