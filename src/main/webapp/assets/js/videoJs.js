$(document).ready(function()
{

    var options = {
        playbackRates: [1, 1.5, 2],
        muted: true,
    };

    video = videojs('videoplay', options);

    $('#overlays-wrap').appendTo($('#videoplay'));

    video.ready(function()
    {

        /*
            this.on('fullscreenchange',function() {
                console.log('fullscreen event');
            });
            */
    }); // end video.ready


    // OVERLAY PREPARE
    // $('#overlays-wrap, .overlay-item').hide();

    // OVERLAY INTERACTION
    $('.radio-tile').click( function() {
        // only show if not yet submitted, prevents submit then click again on radio-tile which would show the vtask-btn-continue
        var submitted = $(this).closest('.overlay-item').hasClass('overlay-submitted');
        if(!submitted)
        {
            $(this).parent().parent().next('.vtask-btn-continue').css('visibility', 'visible');
            // make sure we check the radio button
            $(this).prev('.vtask-choice').prop('checked', true);
        }
    });

    $('.vtask-btn-continue').click( function() {
        // hide continue button
        $(this).css('visibility', 'hidden');

        var overlay = $(this).closest('.overlay-item'); // $(this).parent()
        overlay.hide();

    });

}); // END ready