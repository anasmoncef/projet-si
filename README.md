# Projet Concert

## Réalisation d'une application web permettant de visualiser un concert en choisissant son point de vue

Réalisé par :
- Loïc BLET
- Robin GOUDON
- Yaqi JIANG
- Damien LE GOANVIC
- Steven MARTIN
- Emeric PAIN

Pour fonctionner le projet nécessite :
- Java 8
- MySQL
- Serveur avec un virtualhost http://upload-concert
