COUNTER=1
for f in /$1/*.mov ; do
stabilizedfile="${COUNTER}Stab.mov"
ffmpeg -i "$f" -vf vidstabdetect=stepsize=32:shakiness=10:accuracy=10:result=transform_vectors.trf -f null -
ffmpeg -i "$f" -vf vidstabtransform=input=transform_vectors.trf:zoom=0:smoothing=10,unsharp=5:5:0.8:3:3:0.4 -vcodec libx264 -tune film /$2/$stabilizedfile
COUNTER=$[$COUNTER +1]
done

